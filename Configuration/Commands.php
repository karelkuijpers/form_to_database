<?php /** @noinspection PhpFullyQualifiedNameUsageInspection */
/**
 * This file is part of the "form_to_database" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

return [
    'form_to_database:deleteFormResults' => [
        'class' => \Lavitto\FormToDatabase\Command\DeleteFormResultCommand::class
    ],
];

