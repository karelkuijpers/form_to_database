<?php
/**
 * This file is part of the "form_to_database" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */
    use TYPO3\CMS\Core\Http\ApplicationType;

defined('TYPO3') or die();
//if (TYPO3_MODE === 'BE') {
//if (ApplicationType::fromRequest($request)->isBackend()) {
//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'extTables backend: '."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/form_to_database/Classes/Controller/debug.txt');

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'FormToDatabase',
        'web',
        'formresults',
        'after:FormFormbuilder',
        [
            \Lavitto\FormToDatabase\Controller\FormResultsController::class=> 'index, show, downloadCsv, deleteFormResult, updateItemListSelect, unDeleteFormDefinition'
        ],
        [
            'access' => 'user,group',
            'icon' => 'EXT:form_to_database/Resources/Public/Icons/Extension.svg',
            'labels' => 'LLL:EXT:form_to_database/Resources/Private/Language/locallang_mod.xlf',
            'navigationComponentId' => '',
            'inheritNavigationComponentFromMainModule' => false
        ]
    );
//}
