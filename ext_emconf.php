<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "form_to_database".
 *
 * Auto generated 09-02-2022 15:18
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Form to Database',
  'description' => 'Extends the TYPO3 form with a very simple database finisher, to save the form-results in the database.',
  'category' => 'frontend',
  'constraints' => 
  array (
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'autoload' => 
  array (
    'psr-4' => 
    array (
      'Lavitto\\FormToDatabase\\' => 'Classes',
    ),
  ),
  'state' => 'beta',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearCacheOnLoad' => 0,
  'author' => 'Philipp Mueller',
  'author_email' => 'philipp.mueller@lavitto.ch',
  'author_company' => 'lavitto ag',
  'version' => '11.0.4',
  'clearcacheonload' => false,
);

